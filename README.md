# Functional Testing

## Integraci�n Continua

La pr�ctica de unir (merge) todos los repositorios de desarrollo en un solo varias veces al d�a. El prop�sito principal es evitar problemas de integraci�n. Se utiliza en conjunto con unit tests automatizados, verificando que sean exitosos antes de realizar la integraci�n. Esto previene que el trabajo de un desarrollo arruine el trabajo en proceso de otro. Generalmente se utilizan build servers que corren las pruebas peri�dicamente o en cada commit y reportan los resultados a los desarrolladores. Esto permite llevar un control de calidad sobre el c�digo en el repositorio principal, mejorando la calidad del software mediante pr�cticas especificadas por el equipo de desarrollo, as� como un control continuo en lugar de realizar las pruebas al final del proceso de desarrollo.

## Pir�mide de Pruebas

Es un modelo que utilizan los equipos de desarrollo para mostrar c�mo tres diferentes tipos de pruebas se complementan entre s�. 

![Piramide de pruebas](img/testpmd.png)

La mayor�a de sistemas funcionan en una arquitectura de tres capas. ![Componentes](img/components.png) 

Cada capa equivale a un nivel de la pir�mide. ![Nivel mapeado](img/pyramapping.png)

 * **UI**: Van end-to-end a trav�s de todo el sistema y act�an como un usuario real. Son "costosos" y lentos.
 * **Integration**: No utilizan la interfaz de usuario, realizan pruebas sobre los servicios que consultan las interfaces. Evitan la fragilidad del UI, pero no son muy precisos.
 * **Unit tests**: Pruebas peque�as, at�micas, r�pidas y precisas a nivel de c�digo que permiten al desarrollador saber r�pidamente cosas que no funcionen. Fallan en la integraci�n, pues algunas pruebas requieren interacci�n entre distintos componentes.

## Functional Testing

Es un proceso de QA que basa sus casos de uso en las especificaciones del componente de software que se est� probando. Las funciones son alimentadas con input y se examina el output, sin necesidad de evaluar la estructura interna del proceso. Hay varios tipos:

 * **Smoke testing**: Pruebas para revelar simples fallas lo suficientemente severas para rechazar un software. Por ejemplo, verificar que una p�gina cargue correctamente, o que al hacer click en un bot�n se realice cualquier acci�n.
 * **Sanity testing**: Es una prueba sencilla para evaluar el resultado de una prueba como racional. Por ejemplo, un "Hello, World" es un programa que se crea para verificar que se instal� el ambiente de desarrollo exitosamente.
 * **Regression testing**: Son pruebas que se realizan con el fin de verificar que un programa funciona correctamente al aplicar actualizaciones o parches.
 * **Usability testing**: Pruebas que se enfocan en evaluar un software por medio de sus usuarios, se enfocan en medir la capacidad de cumplir su prop�sito.

## Herramientas Para Pruebas

### Selenium

Es una librer�a para probar aplicaciones Web, por medio de automatizar navegadores de internet. Las pruebas pueden ser ejecutadas contra la mayor�a de navegadores modernos.

```java
package org.openqa.selenium.example;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.support.ui.*;

public class Selenium2Example  {
    public static void main(String[] args) {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.google.com");
        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("Cheese!");
        element.submit();
        System.out.println("Page title is: " + driver.getTitle());
        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getTitle().toLowerCase().startsWith("cheese!");
            }
        });
        System.out.println("Page title is: " + driver.getTitle());
        driver.quit();
    }
}
```

### Cucumber / Gherkin

Cucumber es una herramienta de software para probar componentes de software. Las pruebas se escriben utilizando el lenguaje _Gherkin_, el cual es un lenguaje l�gico que es entendible por los clientes. Cuenta con diversas implementaciones en varios lenguajes como Ruby, PHP, Lua, Java, y otros, as� como integraciones con diversos frameworks como Ruby on Rails, Selenium, Spring y muchos otros m�s.

Cada especificaci�n se escribe en un archivo `.feature` con la sintaxis espec�fica de _Gherkin_.

```
Feature: Refund item

  Scenario: Jeff returns a faulty microwave
    Given Jeff has bought a microwave for $100
    And he has a receipt
    When he returns the microwave
    Then Jeff should be refunded $100
```

